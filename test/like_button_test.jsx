/* global assert, React, ReactTestRenderer */

import LikeButton from '../like_button.jsx'

suite('LikeButton', function() {
    test('smoke', function() {
	let wid = ReactTestRenderer.create(<LikeButton />)
	assert.equal(wid.toJSON().type, 'button')

	wid.toJSON().props.onClick()
	assert.equal(wid.toJSON(), 'You liked this.')
    })
})
