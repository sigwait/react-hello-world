out := _out
web := $(out)/web
all:

vendor.src := react/umd/react.production.min.js \
	react-dom/umd/react-dom.production.min.js
vendor.dest := $(addprefix $(web)/vendor/, $(vendor.src))

$(web)/vendor/%: node_modules/%
	$(copy)

src := .

jsx.src := $(wildcard $(src)/*.jsx)
jsx.dest := $(patsubst $(src)/%.jsx, $(web)/%.js, $(jsx.src))

$(web)/%.js: $(src)/%.jsx
	$(mkdir)
	node_modules/.bin/babel -s true $< -o $@

static.src := $(wildcard $(src)/*.html)
static.dest := $(patsubst $(src)/%, $(web)/%, $(static.src))

$(web)/%: $(src)/%
	$(copy)

compile := $(vendor.dest) $(jsx.dest) $(static.dest)
all: $(compile)



mkdir = @mkdir -p $(dir $@)
define copy =
$(mkdir)
cp $< $@
endef
